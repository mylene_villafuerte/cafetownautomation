package steps;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pages.*;

public
class EmployeeStepdefs {

    WebDriver driver;
    LoginPage LoginPage;
    DashboardPage DashboardPage;
    CreatePage CreatePage;

    @Before
    public void setup(){

        String os =  System.getProperty( "os.name" );
        os = os.toLowerCase();
        String dir = System.getProperty( "user.dir" );
        String url = System.getProperty( "url" );

        if(os.contains( "mac" )) {
            System.setProperty( "webdriver.gecko.driver", dir + "/drivers/mac/geckodriver");
        } else if (os.contains( "windows" )){
            System.setProperty( "webdriver.gecko.driver", dir + "/drivers/windows/geckodriver.exe");
        }

        driver = new FirefoxDriver();
        driver.get(url);

        LoginPage = new LoginPage( driver );
        DashboardPage = new DashboardPage( driver );
        CreatePage = new CreatePage( driver );
    }

    @Given("^I am on cafe town send login page$")
    public void iAmOnCafeTownSendLoginPage() {
        LoginPage.verifyLoginPageTitle();
    }

    @When("^I sign-in using \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iSignInUsingAnd(String user, String pass) {
        LoginPage.setUsernameAndPassword( user, pass );
    }

    @Then("^I should see Cafe TownSend dashboard page$")
    public void iShouldSeeCafeTownSendDashboardPage() {
        DashboardPage.verifyDashboardPage();
    }

    @And("^I select Create button$")
    public void iSelectCreateButton() {
        DashboardPage.selectCreateButton();
    }

    @When("^I populate employee details \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iPopulateEmployeeDetailsAnd(String firstName, String lastName, String startDate, String email) {
        CreatePage.populateEmployeeDetails( firstName, lastName, startDate, email );
    }

    @And("^I click Add button$")
    public void iClickAddButton() {
        CreatePage.selectAddBtn();
    }

    @Then("^I can select Employee \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iCanSelectEmployeeAnd(String firstName, String lastName) throws Throwable {
        DashboardPage.verifyEmployee(firstName, lastName);
    }

    @When("^I click Edit button$")
    public void iClickEditButton() {
        DashboardPage.selectEditButton();
    }

    @Then("^I can see employee details enabled$")
    public void iCanSeeEmployeeDetailsEnabled() {
        CreatePage.verifyFieldsEnabled();
    }

    @And("^I update start date \"([^\"]*)\"$")
    public void iUpdateStartDate(String startDate) throws Throwable {
        CreatePage.populateStartDate( startDate );
    }

    @And("^I select Update button$")
    public void iSelectUpdateButton() {
        CreatePage.selectUpdateBtn();
    }

    @When("^I click Delete button$")
    public void iClickDeleteButton() {
        DashboardPage.selectDeleteButton();
    }

    @And("^I can confirm Yes$")
    public void iCanConfirmYes() {
        DashboardPage.selectYesPopUp();
    }

    @And("^I click Logout button$")
    public void iClickLogoutButton() {
        DashboardPage.selectLogoutButton();
    }

    @After()
    public void After(){
        driver.quit();
    }

}
