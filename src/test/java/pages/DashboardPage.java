package pages;

import gherkin.lexer.Th;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public
class DashboardPage extends Base {

    @FindBy(how = How.ID, using = "greetings")
    static WebElement greetings;

    @FindBy(how = How.ID, using = "bAdd")
    static WebElement btnCreate;

    @FindBy(how = How.ID, using = "bEdit")
    static WebElement btnEdit;

    @FindBy(how = How.ID, using = "bDelete")
    static WebElement btnDelete;

    @FindBy(how = How.XPATH, using = "/html/body/div/header/div/p[1]")
    static WebElement btnLogout;

    private static final String employeeAnna = "//ul[@id='employee-list']/li[normalize-space(text())='%s']";

    public DashboardPage(WebDriver driver) {
        super( driver );
        PageFactory.initElements( driver, this );
    }

    public void verifyDashboardPage() {
        String greetingMessage = greetings.getText();
        Assert.assertEquals( greetingMessage, "Hello Luke" );
    }

    public void selectCreateButton() {
        waitForDrivers( 10 );
        btnCreate.click();
    }

    public void selectDeleteButton(){
        waitForDrivers( 20 );
        btnDelete.click();
    }

    public void selectYesPopUp(){
        waitForDrivers( 10 );
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    public void selectLogoutButton(){
        waitForDrivers( 20 );
        btnLogout.click();
    }
    public void verifyEmployee(String firstName, String lastName) {
        waitForDrivers( 30 );
        String employeeName = firstName + " " + lastName;
        String fullXpath = String.format( employeeAnna, employeeName );
        driver.findElement( By.xpath( fullXpath ) ).click();
    }

    public void selectEditButton(){
        waitForDrivers( 10 );
        btnEdit.click();
    }
}
