package pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public
class LoginPage extends Base {

    @FindBy(how = How.XPATH, using = "//*[@id=\"login-form\"]/fieldset/label[1]/input")
    static WebElement txtUsername;

    @FindBy(how = How.XPATH, using = "//*[@id=\"login-form\"]/fieldset/label[2]/input")
    static WebElement txtPassword;

    @FindBy(how = How.XPATH, using = "//*[@id=\"login-form\"]/fieldset/button")
    private WebElement loginBtn;

    public
    LoginPage(WebDriver driver) {
        super( driver );
        PageFactory.initElements( driver, this );
    }

    public void verifyLoginPageTitle() {
        Assert.assertEquals( driver.getTitle(), "CafeTownsend-AngularJS-Rails" );
        waitForDrivers( 40 );
        verifyFieldsEnabled( txtUsername );
        verifyFieldsEnabled( txtPassword );
    }

    public void setUsernameAndPassword(String username, String password) {
        waitForDrivers(10);
        txtUsername.sendKeys( username );
        txtPassword.sendKeys( password );
        loginBtn.click();
    }

}
