package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public
class CreatePage extends Base {

    @FindBy(how = How.XPATH, using = "/html/body/div/div/div/form/fieldset/label[1]/input")
    static WebElement txtFirstName;

    @FindBy(how = How.XPATH, using = "/html/body/div/div/div/form/fieldset/label[2]/input")
    static WebElement txtLastName;

    @FindBy(how = How.XPATH, using = "/html/body/div/div/div/form/fieldset/label[3]/input")
    static WebElement txtStartDate;

    @FindBy(how = How.XPATH, using = "/html/body/div/div/div/form/fieldset/label[4]/input")
    static WebElement txtEmail;

    @FindBy(how = How.XPATH, using = "/html/body/div/div/div/form/fieldset/div/button[2]")
    static WebElement btnAdd;

    @FindBy(how = How.XPATH, using = "/html/body/div/div/div/form/fieldset/div/button[1]")
    static WebElement btnUpdate;

    @FindBy(how = How.XPATH, using = "/html/body/div/div/div/form/fieldset/div/p")
    static WebElement btnDelete;

    public CreatePage(WebDriver driver) {
        super( driver );
        PageFactory.initElements( driver, this );
    }

    public void populateEmployeeDetails(String firstName, String lastName, String startDate, String email) {
        waitForDrivers( 20 );
        txtFirstName.sendKeys(firstName);
        txtLastName.sendKeys(lastName);
        txtStartDate.sendKeys(startDate);
        txtEmail.sendKeys(email);
    }

    public void populateStartDate(String startDate) {
        txtStartDate.sendKeys(startDate);
    }

    public void selectAddBtn(){
        waitForDrivers( 20 );
        btnAdd.click();
    }

    public void selectUpdateBtn(){
        waitForDrivers( 20 );
        btnUpdate.click();
    }

    public void verifyFieldsEnabled(){
        verifyFieldsEnabled( txtFirstName );
        verifyFieldsEnabled( txtLastName );
        verifyFieldsEnabled( txtStartDate );
        verifyFieldsEnabled( txtEmail );
    }
}
