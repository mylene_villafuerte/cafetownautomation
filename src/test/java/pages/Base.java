package pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public
class Base {
    WebDriver driver;

    public
    Base(WebDriver driver) {
        this.driver = driver;
    }

    public void waitForDrivers(int time) {
        driver.manage().timeouts().implicitlyWait( time, TimeUnit.SECONDS );
    }

    public void verifyFieldsEnabled(WebElement element){
        Assert.assertTrue(element.isEnabled());
    }

}
