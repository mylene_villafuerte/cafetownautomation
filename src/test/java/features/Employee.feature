Feature:
  Employee - add, update and delete
  As a valid user
  I want to be able to login
  In order to view, add, update and delete employees

#  Tester: Mylene Villafuerte

  @regression @create
  Scenario Outline: User login and logout after create employee
    Given I am on cafe town send login page
    When  I sign-in using "Luke" and "Skywalker"
    Then  I should see Cafe TownSend dashboard page
    And   I select Create button
    When  I populate employee details "<firstName>", "<lastName>", "<startDate>" and "<email>"
    And   I click Add button
    Then  I can select Employee "<firstName>" and "<lastName>"
    And   I click Logout button

    Examples:
      | firstName | lastName | startDate  | email         |
      | Anna      | Roberts  | 2019-01-01 | anna@test.com |

  @regression @edit
  Scenario Outline: User login and logout after edit employee start date
    Given I am on cafe town send login page
    When  I sign-in using "Luke" and "Skywalker"
    Then  I should see Cafe TownSend dashboard page
    And   I can select Employee "<firstName>" and "<lastName>"
    When  I click Edit button
    Then  I can see employee details enabled
    And   I update start date "2019-01-20"
    And   I select Update button

    Examples:
      | firstName | lastName |
      | Anna      | Roberts  |

  @regression @delete
  Scenario Outline: User login and logout after delete employee
    Given I am on cafe town send login page
    When  I sign-in using "Luke" and "Skywalker"
    Then  I should see Cafe TownSend dashboard page
    And   I can select Employee "<firstName>" and "<lastName>"
    When  I click Delete button
    And   I can confirm Yes

    Examples:
      | firstName | lastName |
      | Anna      | Roberts  |