import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import pages.Base;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features",
        tags = {"@regression"}, glue = {"steps"},
        plugin = {"pretty", "html:target/cucumber-reports"},
        monochrome = true)

public
class testRunner {

}