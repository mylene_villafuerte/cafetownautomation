$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Employee.feature");
formatter.feature({
  "line": 1,
  "name": "",
  "description": "Employee - add, update and delete\nAs a valid user\nI want to be able to login\nIn order to view, add, update and delete employees",
  "id": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 7,
      "value": "#  Tester: Mylene Villafuerte"
    }
  ],
  "line": 10,
  "name": "User login and logout after create employee",
  "description": "",
  "id": ";user-login-and-logout-after-create-employee",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 9,
      "name": "@regression"
    },
    {
      "line": 9,
      "name": "@create"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "I am on cafe town send login page",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "I sign-in using \"Luke\" and \"Skywalker\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "I should see Cafe TownSend dashboard page",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "I select Create button",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I populate employee details \"\u003cfirstName\u003e\", \"\u003clastName\u003e\", \"\u003cstartDate\u003e\" and \"\u003cemail\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I click Add button",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I can select Employee \"\u003cfirstName\u003e\" and \"\u003clastName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click Logout button",
  "keyword": "And "
});
formatter.examples({
  "line": 20,
  "name": "",
  "description": "",
  "id": ";user-login-and-logout-after-create-employee;",
  "rows": [
    {
      "cells": [
        "firstName",
        "lastName",
        "startDate",
        "email"
      ],
      "line": 21,
      "id": ";user-login-and-logout-after-create-employee;;1"
    },
    {
      "cells": [
        "Anna",
        "Roberts",
        "2019-01-01",
        "anna@test.com"
      ],
      "line": 22,
      "id": ";user-login-and-logout-after-create-employee;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 17486831272,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "User login and logout after create employee",
  "description": "",
  "id": ";user-login-and-logout-after-create-employee;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 9,
      "name": "@regression"
    },
    {
      "line": 9,
      "name": "@create"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "I am on cafe town send login page",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "I sign-in using \"Luke\" and \"Skywalker\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "I should see Cafe TownSend dashboard page",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "I select Create button",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I populate employee details \"Anna\", \"Roberts\", \"2019-01-01\" and \"anna@test.com\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I click Add button",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I can select Employee \"Anna\" and \"Roberts\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click Logout button",
  "keyword": "And "
});
formatter.match({
  "location": "EmployeeStepdefs.iAmOnCafeTownSendLoginPage()"
});
formatter.result({
  "duration": 544766633,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Luke",
      "offset": 17
    },
    {
      "val": "Skywalker",
      "offset": 28
    }
  ],
  "location": "EmployeeStepdefs.iSignInUsingAnd(String,String)"
});
formatter.result({
  "duration": 382967257,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iShouldSeeCafeTownSendDashboardPage()"
});
formatter.result({
  "duration": 38035778,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iSelectCreateButton()"
});
formatter.result({
  "duration": 767761595,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Anna",
      "offset": 29
    },
    {
      "val": "Roberts",
      "offset": 37
    },
    {
      "val": "2019-01-01",
      "offset": 48
    },
    {
      "val": "anna@test.com",
      "offset": 65
    }
  ],
  "location": "EmployeeStepdefs.iPopulateEmployeeDetailsAnd(String,String,String,String)"
});
formatter.result({
  "duration": 1061393607,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iClickAddButton()"
});
formatter.result({
  "duration": 243778969,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Anna",
      "offset": 23
    },
    {
      "val": "Roberts",
      "offset": 34
    }
  ],
  "location": "EmployeeStepdefs.iCanSelectEmployeeAnd(String,String)"
});
formatter.result({
  "duration": 1486560218,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iClickLogoutButton()"
});
formatter.result({
  "duration": 318748137,
  "status": "passed"
});
formatter.after({
  "duration": 862651442,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 25,
  "name": "User login and logout after edit employee start date",
  "description": "",
  "id": ";user-login-and-logout-after-edit-employee-start-date",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 24,
      "name": "@regression"
    },
    {
      "line": 24,
      "name": "@edit"
    }
  ]
});
formatter.step({
  "line": 26,
  "name": "I am on cafe town send login page",
  "keyword": "Given "
});
formatter.step({
  "line": 27,
  "name": "I sign-in using \"Luke\" and \"Skywalker\"",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "I should see Cafe TownSend dashboard page",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "I can select Employee \"\u003cfirstName\u003e\" and \"\u003clastName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I click Edit button",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I can see employee details enabled",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "I update start date \"2019-01-20\"",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I select Update button",
  "keyword": "And "
});
formatter.examples({
  "line": 35,
  "name": "",
  "description": "",
  "id": ";user-login-and-logout-after-edit-employee-start-date;",
  "rows": [
    {
      "cells": [
        "firstName",
        "lastName"
      ],
      "line": 36,
      "id": ";user-login-and-logout-after-edit-employee-start-date;;1"
    },
    {
      "cells": [
        "Anna",
        "Roberts"
      ],
      "line": 37,
      "id": ";user-login-and-logout-after-edit-employee-start-date;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 5444004073,
  "status": "passed"
});
formatter.scenario({
  "line": 37,
  "name": "User login and logout after edit employee start date",
  "description": "",
  "id": ";user-login-and-logout-after-edit-employee-start-date;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 24,
      "name": "@regression"
    },
    {
      "line": 24,
      "name": "@edit"
    }
  ]
});
formatter.step({
  "line": 26,
  "name": "I am on cafe town send login page",
  "keyword": "Given "
});
formatter.step({
  "line": 27,
  "name": "I sign-in using \"Luke\" and \"Skywalker\"",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "I should see Cafe TownSend dashboard page",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "I can select Employee \"Anna\" and \"Roberts\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I click Edit button",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "I can see employee details enabled",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "I update start date \"2019-01-20\"",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I select Update button",
  "keyword": "And "
});
formatter.match({
  "location": "EmployeeStepdefs.iAmOnCafeTownSendLoginPage()"
});
formatter.result({
  "duration": 312116685,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Luke",
      "offset": 17
    },
    {
      "val": "Skywalker",
      "offset": 28
    }
  ],
  "location": "EmployeeStepdefs.iSignInUsingAnd(String,String)"
});
formatter.result({
  "duration": 356614048,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iShouldSeeCafeTownSendDashboardPage()"
});
formatter.result({
  "duration": 41195111,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Anna",
      "offset": 23
    },
    {
      "val": "Roberts",
      "offset": 34
    }
  ],
  "location": "EmployeeStepdefs.iCanSelectEmployeeAnd(String,String)"
});
formatter.result({
  "duration": 1517160710,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iClickEditButton()"
});
formatter.result({
  "duration": 249498105,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iCanSeeEmployeeDetailsEnabled()"
});
formatter.result({
  "duration": 324344581,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2019-01-20",
      "offset": 21
    }
  ],
  "location": "EmployeeStepdefs.iUpdateStartDate(String)"
});
formatter.result({
  "duration": 51750091,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iSelectUpdateButton()"
});
formatter.result({
  "duration": 245808609,
  "status": "passed"
});
formatter.after({
  "duration": 1886686098,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 40,
  "name": "User login and logout after delete employee",
  "description": "",
  "id": ";user-login-and-logout-after-delete-employee",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 39,
      "name": "@regression"
    },
    {
      "line": 39,
      "name": "@delete"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "I am on cafe town send login page",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "I sign-in using \"Luke\" and \"Skywalker\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "I should see Cafe TownSend dashboard page",
  "keyword": "Then "
});
formatter.step({
  "line": 44,
  "name": "I can select Employee \"\u003cfirstName\u003e\" and \"\u003clastName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "I click Delete button",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "I can confirm Yes",
  "keyword": "And "
});
formatter.examples({
  "line": 48,
  "name": "",
  "description": "",
  "id": ";user-login-and-logout-after-delete-employee;",
  "rows": [
    {
      "cells": [
        "firstName",
        "lastName"
      ],
      "line": 49,
      "id": ";user-login-and-logout-after-delete-employee;;1"
    },
    {
      "cells": [
        "Anna",
        "Roberts"
      ],
      "line": 50,
      "id": ";user-login-and-logout-after-delete-employee;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 5365580898,
  "status": "passed"
});
formatter.scenario({
  "line": 50,
  "name": "User login and logout after delete employee",
  "description": "",
  "id": ";user-login-and-logout-after-delete-employee;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 39,
      "name": "@regression"
    },
    {
      "line": 39,
      "name": "@delete"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "I am on cafe town send login page",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "I sign-in using \"Luke\" and \"Skywalker\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "I should see Cafe TownSend dashboard page",
  "keyword": "Then "
});
formatter.step({
  "line": 44,
  "name": "I can select Employee \"Anna\" and \"Roberts\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "I click Delete button",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "I can confirm Yes",
  "keyword": "And "
});
formatter.match({
  "location": "EmployeeStepdefs.iAmOnCafeTownSendLoginPage()"
});
formatter.result({
  "duration": 152781233,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Luke",
      "offset": 17
    },
    {
      "val": "Skywalker",
      "offset": 28
    }
  ],
  "location": "EmployeeStepdefs.iSignInUsingAnd(String,String)"
});
formatter.result({
  "duration": 364984450,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iShouldSeeCafeTownSendDashboardPage()"
});
formatter.result({
  "duration": 29498342,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Anna",
      "offset": 23
    },
    {
      "val": "Roberts",
      "offset": 34
    }
  ],
  "location": "EmployeeStepdefs.iCanSelectEmployeeAnd(String,String)"
});
formatter.result({
  "duration": 1569226378,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iClickDeleteButton()"
});
formatter.result({
  "duration": 67179265,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepdefs.iCanConfirmYes()"
});
formatter.result({
  "duration": 34104370,
  "status": "passed"
});
formatter.after({
  "duration": 1865351300,
  "status": "passed"
});
});