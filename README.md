## PROJECT: Cafe TownSend WEB Test Automation

This is a selenium web automation project for Cafe TownSend website: http://cafetownsend-angular-rails.herokuapp.com/ 
which covers testing of creation, update, delete as well as login and log out using BDD concept and Page Factory. 

Test cases are all available at the Employee.feature file and can be executed at both Mac and Windows using firefox driver.

All codes can be seen under the src/test/java folder.

## Maintenance:

Mainly considered using Page Object Model / Page Factory for organizing Web UI element to make it easier to manage in 
case more  pages will be added. And, In terms of integration I've used Maven for easier integration with Jenkins and Bamboo.

Also, used Behavior Driven Development (BDD) framework using cucumber for easier communication between BAs, Developers 
and Testers, which can also easily be integrated with JIRA defect management and testing tool.

1. Addition of test case can be done by adding scenario at Cucumber Feature file.
2. To maintain latest plugins and dependencies, update the Maven pom.xml file properties section
3. To add or change browser or environment url, update the Maven pom.xml file profile section

## Tech/framework used
 This framework is built with:
 
     - Selenium
     - Cucumber
     - Java
     - Maven
 
## Running the test automation

1. Clone source to a local repository
    git clone https://mylene_villafuerte@bitbucket.org/mylene_villafuerte/cafetownautomation.git 
2. Load project to IDE, preferably IntelliJ
3. Navigate to pom.xml file and import dependencies
4. Once all dependencies are loaded, navigate to Terminal
5. Navigate to automation location then ran the maven command below
    mvn test -Dbrowser=firefox

## Later enhancements
1. Adding chrome browser at the profile that can be ran to both windows and mac
2. Additional scenarios for negative testing
3. Optimize code to lambda java8
